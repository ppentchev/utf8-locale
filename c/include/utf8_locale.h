#ifndef INCLUDED_UTF8_LOCALE_H
#define INCLUDED_UTF8_LOCALE_H
/**
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#if defined(__cplusplus)
extern "C" {
#endif

char *		detect_utf8_locale(const char * const *pref_languages);
char **		get_utf8_vars(const char * const *languages);
char **		get_utf8_env(const char * const *languages);
char **		get_preferred_languages(void);

#if defined(__cplusplus)
}
#endif

#endif
