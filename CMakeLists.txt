# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

cmake_minimum_required(VERSION 3.16.0)

set(U8_VERSION 1.0.1)
set(U8_SOVERSION 0)

project(utf8_locale VERSION ${U8_VERSION})

add_subdirectory(c)
