# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[build-system]
requires = [
  "hatchling >= 1.26, < 2",
  "hatch-requirements-txt >= 0.3, < 0.5",
]
build-backend = "hatchling.build"

[project]
name = "utf8-locale"
description = "Detect a UTF-8-capable locale for running programs in"
readme = "README.md"
license = "BSD-2-Clause"
license-files = ["LICENSES/BSD-2-Clause.txt"]
requires-python = ">= 3.8"
dynamic = ["version"]
classifiers = [
  "Development Status :: 5 - Production/Stable",
  "Intended Audience :: Developers",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3.8",
  "Programming Language :: Python :: 3.9",
  "Programming Language :: Python :: 3.10",
  "Programming Language :: Python :: 3.11",
  "Programming Language :: Python :: 3.12",
  "Programming Language :: Python :: 3.13",
  "Programming Language :: Python :: 3.14",
  "Topic :: Software Development :: Libraries",
  "Topic :: Software Development :: Libraries :: Python Modules",
]

[[project.authors]]
name = "Peter Pentchev"
email = "roam@ringlet.net"

[project.scripts]
u8loc = "utf8_locale.__main__:main"

[project.urls]
Homepage = "https://devel.ringlet.net/devel/utf8-locale/"
Changes = "https://devel.ringlet.net/devel/utf8-locale/changes/"
"Issue Tracker" = "https://gitlab.com/ppentchev/utf8-locale/-/issues"
"Source Code" = "https://gitlab.com/ppentchev/utf8-locale"

[tool.hatch.build]
include = [
  "/.reuse",
  "/.editorconfig",
  "/LICENSES",
  "/NEWS.md",
  "/nix",
  "/pyproject.toml",
  "/python",
  "/README.md",
  "/setup.cfg",
  "/tests",
  "/tox.ini",
  "/u8loc.1",
]

[tool.hatch.build.targets.wheel]
packages = ["python/utf8_locale"]

[tool.hatch.version]
path = "python/utf8_locale/detect.py"

[tool.black]
line-length = 100

[tool.mypy]
strict = true

[tool.publync.format.version]
major = 0
minor = 1

[tool.publync.build.tox]

[tool.publync.sync.rsync]
remote = "marla.ludost.net:vhosts/devel.ringlet.net/public_html/devel/utf8-locale"

[tool.ruff]
extend = "ruff-base.toml"
output-format = "concise"
preview = true

[tool.ruff.lint]
select = ["ALL"]

[tool.test-stages]
stages = [
  "(@check or @docs) and @quick and not @manual",
  "(@check or @docs) and not @manual",
  "@tests and not @manual",
]
