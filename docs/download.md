<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [utf8-locale](index.md) available for download.

## [1.0.4] - 2025-01-18

### Source tarball

- [utf8-locale-1.0.4.tar.gz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.4.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.4.tar.gz.asc))
- [utf8-locale-1.0.4.tar.bz2](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.4.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.4.tar.bz2.asc))
- [utf8-locale-1.0.4.tar.xz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.4.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.4.tar.xz.asc))

## [1.0.3] - 2024-02-28

### Source tarball

- [utf8-locale-1.0.3.tar.gz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.3.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.3.tar.gz.asc))
- [utf8-locale-1.0.3.tar.bz2](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.3.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.3.tar.bz2.asc))
- [utf8-locale-1.0.3.tar.xz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.3.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.3.tar.xz.asc))

## [1.0.1] - 2023-06-29

### Source tarball

- [utf8-locale-1.0.1.tar.gz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.1.tar.gz.asc))
- [utf8-locale-1.0.1.tar.bz2](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.1.tar.bz2.asc))
- [utf8-locale-1.0.1.tar.xz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.1.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.1.tar.xz.asc))

## [1.0.0] - 2022-10-30

### Source tarball

- [utf8-locale-1.0.0.tar.gz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.0.tar.gz.asc))
- [utf8-locale-1.0.0.tar.bz2](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.0.tar.bz2.asc))
- [utf8-locale-1.0.0.tar.xz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-1.0.0.tar.xz.asc))

## [0.3.0] - 2022-02-20

### Source tarball

- [utf8-locale-0.3.0.tar.gz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-0.3.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-0.3.0.tar.gz.asc))
- [utf8-locale-0.3.0.tar.bz2](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-0.3.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-0.3.0.tar.bz2.asc))
- [utf8-locale-0.3.0.tar.xz](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-0.3.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/utf8-locale/utf8-locale-0.3.0.tar.xz.asc))

[1.0.4]: https://gitlab.com/ppentchev/utf8-locale/-/tags/release%2F1.0.4
[1.0.3]: https://gitlab.com/ppentchev/utf8-locale/-/tags/release%2F1.0.3
[1.0.1]: https://gitlab.com/ppentchev/utf8-locale/-/tags/release%2F1.0.1
[1.0.0]: https://gitlab.com/ppentchev/utf8-locale/-/tags/release%2F1.0.0
[0.3.0]: https://gitlab.com/ppentchev/utf8-locale/-/tags/release%2F0.3.0
