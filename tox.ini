# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[tox]
minversion = 4.1
envlist =
  ruff
  format
  mypy
  reuse
  docs
  unit_tests
  functional
isolated_build = True

[defs]
pyfiles =
  python/utf8_locale \
  python/unit_tests \
  tests

[testenv:ruff]
skip_install = True
tags =
  check
  quick
deps =
  -r python/requirements/ruff.txt
commands =
  ruff check -- {[defs]pyfiles}

[testenv:unit_tests]
tags =
  tests
deps =
  -r python/requirements/tests.txt
commands =
  pytest {posargs}

[testenv:mypy]
skip_install = True
tags =
  check
deps =
  -r python/requirements/tests.txt
  mypy >= 1, < 2
commands =
  mypy {[defs]pyfiles}

[testenv:format]
skip_install = True
tags =
  check
  quick
deps =
  -r python/requirements/ruff.txt
commands =
  ruff check --config ruff-base.toml --select=I --diff -- {[defs]pyfiles}
  ruff format --check --config ruff-base.toml --diff -- {[defs]pyfiles}

[testenv:reformat]
skip_install = True
tags =
  format
  manual
deps =
  -r python/requirements/ruff.txt
commands =
  ruff check --config ruff-base.toml --select=I --fix -- {[defs]pyfiles}
  ruff format --config ruff-base.toml -- {[defs]pyfiles}

[testenv:functional]
tags =
  tests
deps =
  -r python/requirements/tests.txt
commands =
  python3 tests/functional.py -p {envdir}/bin/u8loc

[testenv:reuse]
skip_install = True
tags =
  check
  quick
deps =
  reuse >= 4, < 5
commands =
  reuse lint

[testenv:pyupgrade]
skip_install = True
tags =
  check
  manual
deps =
  pyupgrade >= 3, < 4
allowlist_externals =
  find
commands =
  find python tests -type f -name '*.py' -exec pyupgrade --py38-plus -- '{}' +

[testenv:docs]
skip_install = True
tags =
  docs
deps =
  -r python/requirements/docs.txt
commands =
  mkdocs build
