#![deny(missing_docs)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! utf8-locale: detect a UTF-8-capable locale for running child processes in.
//!
//! Sometimes it is useful for a program to be able to run a child process and
//! more or less depend on its output being valid UTF-8. This can usually be
//! accomplished by setting one or more environment variables, but there is
//! the question of what to set them to - what UTF-8-capable locale is present
//! on this particular system? This is where the `utf8_locale` module comes in.
#![doc(html_root_url = "https://docs.rs/utf8-locale/1.0.4")]
#![allow(clippy::pub_use)]

pub mod detect;

pub use detect::{LanguagesDetect, UErr as Error, Utf8Detect, Utf8Environment};

#[cfg(test)]
pub mod tests;
